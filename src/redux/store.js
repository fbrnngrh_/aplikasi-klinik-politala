import {legacy_createStore as createStore} from 'redux'; // eslint-disable-line

const initialState = {
  loading: false,
  name: 'febrian',
  address: 'Pelaihari',
};
const reducer = (state = initialState, action) => {
  if (action.type === 'SET_LOADING') {
    return {...state, loading: action.value};
  }
  return state;
};

const store = createStore(reducer);

export default store;
